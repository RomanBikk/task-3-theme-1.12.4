import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        try {
            int num = Integer.parseInt(reader.readLine());
            int factorial = 1;
            while (true){
                factorial *=num;
                num -=1;
                if(num<1) break;
            }
            System.out.println(factorial);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
